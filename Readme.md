# Demo project

## Setup

This repo contains the stack as we need it. Please do the following

In terminal change into the directory.
If you use nvm (Node Version Manager) use

    nvm use

If not - please see .nvmrc - it contains the node version this stack has been made for.

Then install the required node packages:

    npm install
    
All source files are in the 'src' directory.  

./html (the html templates)

./styles (the custom scss files)

./js (main javascript file)    


In gulpfile.js there are som pre-defined tasks.   
The default task does all the job in once
Just run
    gulp
    
For all parts (html,scss,js) there is a "watcher".  
Simple run

    gulp watch
    
 and all source directoties will be observed for changes.
 
 The destination files will be compiled to the ./build directory.
 
### scripts 
Please note the 'scripts' gult task.  
The required js-files will be concatenated in the following order:
 
   1. jquery.js
   2. bootstrap.js
   3. /src/js/main.js

All javascript will be compiled to one file: /build/javascript/main.js (and a gz version of that file)
   
### html
In case we need more than 1 html template, please make use of the "partials".  
Please see ./src/index.html  - note the partial tags

### styles
all custom styling has to be done in ./src/styles/app.scss  
Overwriting on bootstrap variables has to be done in ./src/sryles/variables.scss  
This file is loaded prior to the boostrap core files, so colors, fonts and can be customized.
All (s)css will be compiled to one file: /build/css/main.css (and a gz version of that file)

### When finished
Please send us a zip (maybe without the node_modules) folder or a link to your fork of this repo.

For all kind of questions please send a email to: robert@kohlenberg.info

Robert Schwandner

     


