const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const gulp = require('gulp');
const gzip = require('gulp-gzip');
const htmlPartial = require('gulp-html-partial');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');


const config = {
	htmlPath: 'src/html',
	stylesPath: 'src/styles',
	jsPath: 'src/js',
	outputDir: './build'
}
 
gulp.task('html', function () {
    return gulp.src(['src/html/*.html'])
        .pipe(htmlPartial({
            basePath: 'src/html/_partials/'
        }))
        .pipe(gulp.dest(config.outputDir));
});

gulp.task('styles', function () {
    return gulp.src(config.stylesPath + '/main.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                config.stylesPath,
                './node_modules/bootstrap-scss',
                './node_modules/font-awesome/scss'
            ]
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.outputDir + '/css'))
        .pipe(gzip())
        .pipe(gulp.dest(config.outputDir + '/css'))
        ;
});

gulp.task('scripts', function () {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/bootstrap/dist/js/bootstrap.js',
        config.jsPath + '/main.js',
    ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.outputDir + '/javascript'))
        .pipe(gzip())
        .pipe(gulp.dest(config.outputDir + '/javascript'))
        ;
});

gulp.task('icons', function () {
    return gulp.src('./node_modules/font-awesome/fonts/**.*')
        .pipe(gulp.dest(config.outputDir + '/fonts'));
});


gulp.task('default', 
	gulp.parallel(['html', 'styles', 'scripts', 'icons'])	
);

gulp.task('watch', function () {
    gulp.watch([config.stylesPath + '**/*.scss', config.stylesPath + '**/*.sass', config.stylesPath + '**/*.css'], gulp.series(['styles']));
    gulp.watch([config.jsPath + '**/*.js'], gulp.series(['scripts']));
    gulp.watch([config.htmlPath + '/**/*'], gulp.series(['html']));
});